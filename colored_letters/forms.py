from django import forms


class UploadFileForm(forms.Form):
    input_text = forms.CharField(
        widget=forms.Textarea(attrs={'rows': 30, 'cols': 100}))
    font_size = forms.IntegerField(label='Font size')
    colors_file = forms.FileField(initial=10)
