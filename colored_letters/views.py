import json

from django.http import JsonResponse
from django.views.generic import TemplateView

from colored_letters import forms


class Main(TemplateView):
    template_name = 'main.html'

    def post(self, request, *args, **kwargs):

        input_text = request.POST['input_text']
        letters_colors = json.loads(request.POST['colors'])
        font_size = request.POST['font_size']

        output_text = '<font size="{}">'.format(font_size)
        for letter in input_text:
            if letter == '\n':
                output_text += '<p></p>'
                continue

            if letter.isalpha() and letter.lower() in letters_colors:
                output_text += '<span style="color:#{}">{}</span>'.format(
                    letters_colors[letter.lower()], letter)
            else:
                output_text += letter

        output_text += '</font>'

        return JsonResponse(
            {'output_text': output_text})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = forms.UploadFileForm()

        return context
