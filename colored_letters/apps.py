from django.apps import AppConfig


class ColoredLettersConfig(AppConfig):
    name = 'colored_letters'
